package main;

public class Main {
	public static void main(String[] args) {
		 /*Java keyword		Data Type		Examples			Min					Max
		 int				integer			422, -13, 0			-2^31				2^31-1
		 double				real number		-23.1, 14.56, 		9.4e3-1.8x10^-38	-1.8x10^38
		 char				one character	'A', '1', 'z', '%'	NA					NA
		 boolean			true or false	true, false			NA					NA
		 */

		 int x = 10;
		 System.out.println("My x contains: " + x);
		 x = 15;
		 System.out.println("My x contains: " + x);
		 String myString = "My x contains: ";
		 System.out.println(myString + x);
		 
		 calculate(null);
		 stringConcatenation(args);
	 }
	 
	 public static void calculate(String[] args) {
		 double x = 10;
		 int y = 3;
		 System.out.println("x + y = " + (x+y));
		 System.out.println("x - y = " + (x-y));
		 System.out.println("x * y = " + (x*y));
		 System.out.println("x / y = " + (x/y));
		 x++;
		 y *= 10;
		 System.out.println("x = " + x);
		 System.out.println("y = " + y);
	 }
	 
	 public static void stringConcatenation(String[] args) {
		 String text1 = "Hello";
		 String text2 = "World";
		 
		 System.out.println(text1 + text2);
		 System.out.println(text1 + " " + text2);
		 System.out.println(text1 + 12345);
	}
}
